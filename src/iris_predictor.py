#################################################################################
#                                                                               #
# Copyright 2016 Luca Antonelli                                                 #
# luke.anto@gmail.com                                                           #
#                                                                               #
# This file is part of Iris Predictor.                                          #
#                                                                               #
# Iris Predictor is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by          #
# the Free Software Foundation, either version 3 of the License, or             #
# (at your option) any later version.                                           #
#                                                                               #
# Iris Predictor is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 #
# GNU General Public License for more details.                                  #
#                                                                               #
# You should have received a copy of the GNU General Public License             #
# along with Iris Predictor.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                               #
#################################################################################

# Script used to check Iris Predictor

import os
import numpy as np

from data_manager import DataLoader

from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelEncoder, StandardScaler

from sklearn.cross_validation import train_test_split


from sklearn.linear_model import SGDClassifier, LogisticRegression
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier , BaggingClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import AdaBoostClassifier
from sklearn.decomposition import KernelPCA

from classifier import IrisClassifier
from data_manager import IrisDataLoader

# Step 0: Base variables
# Random seed (set None to have every time different results)
RANDOM_STATE = 0
# Output dir
OUT_DIR = "output"
if not os.path.exists(OUT_DIR):
  os.makedirs(OUT_DIR)
else:
  for f in [ f for f in os.listdir(OUT_DIR)]:
    os.remove(os.path.join(OUT_DIR, f))

# Step 1: Load Data
data = IrisDataLoader().load(os.path.join("data", "iris.data"))

# Step 2: extract data for training and testing
X = data.iloc[:,:-1]
y = data.iloc[:,-1]

# Step 3: Convert iris class labels
labelEncoder = LabelEncoder()
labelEncoder.fit(y)
y = labelEncoder.transform(y)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = RANDOM_STATE, stratify = y)

# Step 4: First Comparison of Classifier with Nested Cross Validation

# Collect algorithms to test
algorithms = []

# Support Vector Machine
svm = Pipeline(steps = [('scl', StandardScaler()),
                        ('svm', SVC(random_state = RANDOM_STATE, degree = 5)),])
algorithms.append({'name': 'svm',
                   'estimator': svm,
                   'paramGrid': {'svm__C': np.logspace(-4, 1, num = 6, base = 10),
                                 'svm__kernel': ['poly', 'rbf', 'linear', 'sigmoid']}
                  })


# K Nearest Neighbors
knn = Pipeline(steps = [('scl', StandardScaler()),
                        ('knn', KNeighborsClassifier()),])
algorithms.append({'name': 'knn',
                   'estimator': knn,
                   'paramGrid': {'knn__p': [1, 2],
                                 'knn__weights': ['uniform', 'distance']}
                  })

# Decision Tree
decisionTree = Pipeline(steps = [('tree', DecisionTreeClassifier(random_state = RANDOM_STATE))])
algorithms.append({'name': 'decisionTree',
                   'estimator': decisionTree,
                   'paramGrid': {'tree__max_depth': [3, 4, 5, 6, 7, 8, 9],
                                 'tree__class_weight': [None, 'balanced']}
                  })

# Random Forest
randomForest = Pipeline(steps = [('rndf', RandomForestClassifier(random_state = RANDOM_STATE))])
algorithms.append({'name': 'randomForest',
                   'estimator': randomForest,
                   'paramGrid': {'rndf__n_estimators': [10, 20],
                                 'rndf__criterion': ['gini', 'entropy'],
                                 'rndf__max_depth': [3, 4, 5, 6, 7, 8, 9],
                                 'rndf__class_weight': [None, 'balanced', 'balanced_subsample']}
                  })

# Naive Bayes
bayes = Pipeline(steps = [('bay', MultinomialNB())])
algorithms.append({'name': 'bayes',
                   'estimator': bayes,
                   'paramGrid': {'bay__fit_prior': [False, True]}
                  })

# SGD - Linear Support Vector Machine
SGDLinearSVM = Pipeline(steps = [('scl', StandardScaler()),
                                 ('clf', SGDClassifier(loss='hinge', random_state = RANDOM_STATE)),])
algorithms.append({'name': 'SGDLinearSVM',
                   'estimator': SGDLinearSVM,
                   'paramGrid': {'clf__alpha': np.logspace(-7, 3, num = 11, base = 10) }
                  })

# SGD - Linear Support Vector Machine with Quadratic Penalization
SGDLinearSVMSquared = Pipeline(steps = [('scl', StandardScaler()),
                                        ('clf', SGDClassifier(loss='squared_hinge', random_state = RANDOM_STATE)),])
algorithms.append({'name': 'SGDLinearSVMSquared',
                   'estimator': SGDLinearSVMSquared,
                   'paramGrid': {'clf__alpha': np.logspace(-7, 3, num = 11, base = 10) }
                  })

# SGD - Logistic Regression
SGDLogisticRegression = Pipeline(steps = [('scl', StandardScaler()),
                                          ('clf', SGDClassifier(loss='log', random_state = RANDOM_STATE)),])
algorithms.append({'name': 'SGDLogisticRegression',
                   'estimator': SGDLogisticRegression,
                   'paramGrid': {'clf__alpha': np.logspace(-7, 3, num = 11, base = 10) }
                  })

adaBoost = Pipeline(steps = [('scl', StandardScaler()),
                             ('clf', AdaBoostClassifier(random_state = RANDOM_STATE, algorithm = "SAMME")),])
algorithms.append({'name': 'adaBoost',
                   'estimator': adaBoost,
                   'paramGrid': {'clf__base_estimator': [SGDClassifier(loss='squared_hinge', random_state = RANDOM_STATE)],
                                 'clf__n_estimators': [50, 100, 200],
                                 'clf__learning_rate': [0.1, 1.0]}
                  })

classifier = IrisClassifier(labelEncoder.classes_)
#classifier.nestedCrossValidation(algorithms, X_train, y_train, outDir = OUT_DIR)

# Step 6: Parameter tuning for best algorithms
algorithms = []
algorithms.append({'name': 'randomForest',
                   'estimator': randomForest,
                   'paramGrid': {'rndf__n_estimators': np.linspace(10, 20, num = 3, dtype = 'int'),
                                 'rndf__criterion': ['gini', 'entropy'],
                                 'rndf__max_depth': np.linspace(3, 11, num = 9, dtype = 'int'),
                                 'rndf__class_weight': [None, 'balanced', 'balanced_subsample']}
                  })
algorithms.append({'name': 'decisionTree',
                   'estimator': decisionTree,
                   'paramGrid': {'tree__max_depth': [3, 4, 5, 6, 7, 8, 9],
                                 'tree__class_weight': [None, 'balanced'] }
                  })
algorithms.append({'name': 'bayes',
                   'estimator': bayes,
                   'paramGrid': {'bay__fit_prior': [False, True],
                                 'bay__alpha': np.logspace(-3,1, num = 12, base = 10)}
                  })
algorithms.append({'name': 'svm',
                   'estimator': svm,
                   'paramGrid': {'svm__C': np.logspace(-4, 1, num = 12, base = 10),
                                 'svm__kernel': ['poly', 'rbf', 'linear', 'sigmoid'],
                                 'svm__degree': [2,3,4],
                                 'svm__shrinking': [True, False]}
                  })

bestEstimators = []
for algorithm in algorithms:
  algorithm['bestEstimator'] = classifier.crossValidation(algorithm, X_train, y_train, outDir = OUT_DIR)

# Checking best algorithms metrics
for algorithm in algorithms:
  classifier.metrics(algorithm['bestEstimator'], algorithm['name'], X_train, y_train, X_test, y_test, outDir = OUT_DIR)
  classifier.learningCurve(algorithm['bestEstimator'], algorithm['name'], X, y, outDir = OUT_DIR)

# Trying Bagging on algorithms
baggingAlgorithms = []
for algorithm in algorithms:
  baggingAlgorithm = {'name' : 'bagging_' + algorithm['name'],
                       'algorithm' : BaggingClassifier(base_estimator = algorithm['bestEstimator'], random_state = RANDOM_STATE)}
  baggingEstimator = Pipeline(steps = [('bag', baggingAlgorithm['algorithm'])])
  classifier.metrics(baggingEstimator,
                     baggingAlgorithm['name'], 
                     X_train, y_train, X_test, y_test, outDir = OUT_DIR)
  classifier.learningCurve(baggingEstimator, algorithm['name'], X, y, outDir = OUT_DIR)
  baggingAlgorithms.append(baggingAlgorithm)

