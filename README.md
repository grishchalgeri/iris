Try to predict the type of iris plant

The program use scikit-learn Python module (http://scikit-learn.org/)

Data file are extracted from UCI Machine Learning repository (Lichman, M. (2013). UCI Machine Learning Repository [http://archive.ics.uci.edu/ml]. Irvine, CA: University of California, School of Information and Computer Science.)


See http://archive.ics.uci.edu/ml/datasets/Iris for a description of data.

See COPYING for distribution licence.

== Python packages required ==

* scikit-learn: Machine learning for Python
* NumPy: 
* pandas: data manipulation
* Matplotlib: Data plotting
* tabulate: pretty prints tables

== Usage ==
For Linux (and probably Mac) you can launch ./run.sh via terminal
For all platforms, launch "python3 src/diabetes_predictor.py" from tha home dir of the project.

Copyright 2016 Luca Antonelli
luke.anto@gmail.com

